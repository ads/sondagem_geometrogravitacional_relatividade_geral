\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[QX]{fontenc}
\usepackage{hyperref}
\usepackage[english,brazil]{babel}
\usepackage{amsmath,amssymb,amsthm}
\usepackage[retainorgcmds]{IEEEtrantools}
\usepackage{indentfirst}		% indenta o primeiro parágrafo de cada seção
\usepackage{microtype} 			% para melhorias de justificação
\usepackage{multirow}
\usepackage{tabulary}

\theoremstyle{definition}
\newtheorem{ideia}{Ideia}

\theoremstyle{plain}
\newtheorem{conjectura}{Conjectura}

\newcommand{\RR}{\mathbb{R}}
\newcommand{\til}[1]{\widetilde{#1}}

\author{Dr. Alexandre de Sousa Mota}
\date{Outubro 2023}
\title{Superfícies mínimas de bordo livre \\ e Relatividade Geral}
\hypersetup{
  pdfauthor={Dr. Alexandre de Sousa Mota},
  pdftitle={Superfícies mínimas de bordo livre e Relatividade Geral},
  pdfkeywords={},
  pdfcreator={},
  pdflang={Portuguese}{Português}}

\begin{document}

\selectlanguage{brazil} % seleciona o idioma do documento (conforme pacotes do babel)
\frenchspacing % retira espaço extra obsoleto entre as frases

% ---
% capa
% ---
\makeatletter
\begin{titlepage}
  \begin{center}
    \LARGE{PROJETO DE PESQUISA \\ PESQUISADOR DE PÓS-DOUTORADO}
  \end{center}

  \vfill
  \begin{center}
    \LARGE \bf \@title \\
    \vspace{1cm}
    \large \@author \\
  \end{center}

  \vfill
  \begin{center}
    {\large \@date}
  \end{center}

\end{titlepage}
\makeatother

% ---
% contracapa
% ---
\makeatletter
\begin{titlepage}
  \begin{center}
    \LARGE{PROJETO DE PESQUISA \\ PESQUISADOR DE PÓS-DOUTORADO}
  \end{center}


  \vfill
  \begin{center}
    \LARGE \bf \@title \\
    \vspace{1cm}
    \large \@author \\
  \end{center}

  \vspace{1cm}
  \begin{flushright}
    \begin{minipage}[c]{8cm}
      Proposta apresentada ao Programa de Pós-Graduação em Matemática,
      do Departamento de Matemática, da Universidade Federal do Ceará
      -- \href{https://pgmat.ufc.br}{Pgmat/UFC}, em atendimento
      a chamada pública do
      \href{http://montenegro.funcap.ce.gov.br/sugba/editais/}
      {Edital 10/2023}, da Fundação Cearense de Amparo a Pesquisa
      -- \href{https://www.funcap.ce.gov.br/}{Funcap},
      para apresentação de propostas para bolsas de Pesquisador Visitante.
    \end{minipage}
  \end{flushright}

  \vfill
  \begin{center}
    {\large \@date}
  \end{center}

\end{titlepage}
\makeatother

\tableofcontents

\section{Apresentação do tema da proposta}

Uma das maiores conquistas da ciência no século XX foi a Teoria da Relatividade
Geral proposta por Albert Einstein. Uma teoria física bem sucedida, que modela a
estrutura macroscópica do nosso universo. Nessa teoria o espaço e o tempo são
combinados em um objeto geométrico chamado \emph{espaço-tempo}, que é modelado
por uma variedade quadrimensional \(\overline{M}^4\), munida de uma métrica
lorentziana \(\overline{g}\), que modela o \emph{campo gravitacional}, e uma
noção geométrica de orientação do tempo, que codifica geometricamente as noções
intuitivas de passado e futuro; o conteúdo de energia e momento da matéria é
descrito por uma forma bilinear simétrica e livre de divergência \(\overline{T}\)
sobre a variedade \(\overline{M}\), denominada \emph{tensor energia-momento}, que
depende da métrica \(\overline{g}\) e dos campos não-gravitacionais, por vezes
também chamados de \emph{campos de matéria}, aqui denotados coletivamente pelo
símbolo \(\overline{\mathcal{F}}\), que, por sua vez, são modelados como sendo
seções de fibrados sobre o espaço-tempo \(\overline{M}\), escolhidos
adequadamente, conforme o tipo de matéria que se pretenda modelar, ou seja,
\(\overline{T} \equiv \overline{T}(\overline{g}, \overline{\mathcal{F}})\).
A propriedade de ser livre de divergência codifica o princípio físico da
conservação local da energia e do momento.

Os espaços-tempos de interesse para a teoria são aqueles que realizam a
\emph{equação de Einstein}, à saber:
\begin{align*}
  \overline{G} + \Lambda \overline{g} = 8\pi \overline{T},
\end{align*}
onde \(\Lambda\) é uma constante real, denominada \emph{constante cosmológica}, e
\begin{align*}
  \overline{G} := \overline{Rc} - \frac{\overline{Sc}}{2} \overline{g}
\end{align*}
é o \emph{tensor de Einstein}, uma forma bilinear simétrica e livre de
divergência, que contém a mesma informação sobre a curvatura do espaço-tempo que
a curvatura de Ricci \(\overline{Rc}\) da métrica \(\overline{g}\). Nesta teoria,
a gravidade é interpretada como sendo uma manifestação da curvatura do
espaço-tempo, induzida pelo campo gravitacional em resposta a distribuição da
matéria. O caso particular em que o tensor energia-momento é trivial, isto é,
\(\overline{T} \equiv 0\), os espaços-tempos de interesse modelam o \emph{vácuo}
e uma característica importante desta teoria é que existem soluções
\((\overline{M}, \overline{g})\) não-triviais que realizam a equação mesmo nesta
situação, ou seja, a curvatura de Riemann da métrica \(\overline{g}\) não é
identicamente nula.

Por ser uma teoria com uma formulação geométrica cristalina, é frequente que
objetos geométricos conhecidos na matemática emerjam nesse contexto e estejam
munidos de uma interpretação física relevante. Por outro lado, os fenômenos
gravitacionais que emergem na teoria, costumam prover argumentos e construções
de objetos geométricos, que apontam investigações matematicamente relevantes.
Essa simbiose geometrogravitacional dá um caráter distinto a essas investigações,
haja visto que são de interesse interdisciplinar para ambas as comunidades
das áreas de matemática e física e, certas vezes, influencia o desenvolvimento de
outras áreas, a exemplo da filosofia~\cite{Arminjonspacetimephysicalspace2017}.
É nesse contexto egrégio onde as atividades dessa proposta se situam.

\subsection{Conjunto de dados iniciais e superfícies mínimas}

Nessa simbiose geometrogravitacional, destaca-se o papel desempenhado pelas
\emph{superfícies mínimas}, isto é, superfícies imersas em uma variedade
riemanniana, cuja curvatura média é identicamente nula. Na teoria da
Relatividade Geral, essas superfícies costumam ocorrer nos ditos conjuntos de
dados iniciais, que emergem na formulação de valor inicial dessa teoria e que
agora passamos a descrevê-los.

Considere uma hipersuperfície tipo-espaço \(M^3\), orientada e mergulhada em um
espaço-tempo \((\overline{M}^4, \overline{g})\), isto é, uma hipersuperfície
orientada e mergulhada, cuja métrica induzida \(g\) é positiva definida, ou seja,
é uma métrica riemanniana. A informação sobre a geometria extrínseca de uma
hipersuperfície orientada, está contida na sua segunda forma fundamental \(K\),
que é uma forma bilinear simétrica sobre a hipersuperfície. A tripla \((M,g,K)\)
costuma ser interpretada como um modelo geométrico para o nosso universo,
conforme registrado por alguma família de observadores, em um certo instante
de tempo. Se o espaço-tempo realiza a Equação de Einstein, as equações de Gauss
e Codazzi para subvariedades garantem que a tripla \((M, g, K)\) satisfaz as
\emph{equações de vínculos}, à saber:
\begin{align*}
Sc_g + (\mathrm{tr}_gK)^2 -||K||^2_g - \Lambda &= 16\pi \mu \\
\mathrm{div}_g \left( K - (\mathrm{tr}_g K)g \right) &= 8\pi J \\
\mathcal{C}(\mathcal{F},g) &= 0
\end{align*}
onde \(\mu = T(n,n) \in C^{\infty}(M)\) e
\(J = T(n, \cdot) \in \Gamma\left( T^*M \right)\) denotam
a \emph{densidade (pontual) de energia} e a \emph{densidade de momento} dos
campos de matéria, respectivamente, com \(n \in \Gamma\left( T^{\perp}M \right)\)
indicando o campo normal unitário ao longo de \(M\), orientado para o futuro;
\(\mathcal{C}(\mathcal{F},g)\) denota o conjunto adicional de vínculos que devem
ser satisfeitos pelos campos de matéria ao longo da hipersuperfície \(M\),
usualmente modelados por seções de fibrados sobre \(M\) e aqui denotados
coletivamente pelo símbolo \(\mathcal{F}\).

Reciprocamente, dado uma variedade riemanniana tridimensional \((M^3,g)\),
orientada e dotada de uma forma bilinear simétrica \(K\), a solução do problema
de valor inicial em Relatividade Geral implica que se a tripla \((M^3,g,K)\)
satisfaz as equações de vínculos, então existe um espaço-tempo
\((\overline{M}^4, \overline{g})\) que realiza a equação de Einstein para algum
tensor energia-momento \(\overline{T}\) e constante cosmológica \(\Lambda\), tal
que a variedade \(M\) é realizada como uma hipersuperfície tipo-espaço orientada
e mergulhada nesse espaço-tempo, com métrica induzida \(g\) e segunda forma
fundamental \(K\). Ademais, a solução do problema de valor inicial também
estabelece uma assertiva acerca da unicidade deste espaço-tempo. Assim,
uma tupla \((M^3,g,K,\mu,J)\), que satisfaz as equações de vínculo, recebe
a alcunha de \emph{conjunto de dados iniciais} ou \emph{dados de Cauchy}
e o espaço-tempo associado \((\overline{M}, \overline{g})\), que realiza
a equação de Einstein, é dito ser o \emph{desenvolvimento de Cauchy}
do conjunto de dados iniciais. Os conjuntos de dados iniciais de interesse em
relatividade geral costumam ser aqueles que satisfazem a
\emph{condição de energia dominante}, isto é, \(\mu \ge |J|_g\).

No caso especial em que um conjunto de dados iniciais \((M^3,g,K)\) é totalmente
geodésico no seu desenvolvimento de Cauchy, isto é, \(K \equiv 0\), ele é dito
ser \emph{simétrico no tempo} e as equações de vínculos, combinadas com condição
da energia dominante, se traduzem na seguinte condição geométrica sobre a
curvatura escalar:
\begin{align*}
  Sc_g \ge \Lambda.
\end{align*}
Assim, nesse caso especial, os conjuntos de dados iniciais de interesse são
modelados por variedades riemannianas tridimensionais \((M^3, g)\) de curvatura
escalar limitada inferiormente.

É um fato conhecido na literatura que as
\emph{superfícies mínimas compactas, sem bordo e estáveis},
imersas em conjuntos de dados iniciais simétricos no tempo, podem ser usadas
parar modelar os \emph{horizontes aparentes} de buracos negros. Tais horizontes
são uma versão \emph{quasi-local} do horizonte de eventos de um buraco negro,
onde quasi-local se refere a um conceito que depende apenas da geometria na
vizinhança de uma subvariedade. Também é conhecido que tais superfícies
contribuem quantitativamente para energia de sistemas gravitacionais isolados.
De fato, esse é o conteúdo da versão riemanniana da
\emph{conjectura de Penrose}, que é conhecida ser válida em dimensão 3.

Nos últimos anos houve um grande desenvolvimento na teoria da existência de
superfícies mínimas compactas, sem bordo e
instáveis~\cite{MarquesApplicationsMinMax2020}, bem como na teoria de superfícies
mínimas compactas e com bordo
livre~\cite{GuangMinmaxtheory2021,LiMinmaxtheoryfree2021}, usando a chamada
\emph{teoria Min-Max}. O proponente, em colaboração com Vanderson Lima (UFGRS)
e Tiarlos Cruz (UFAL), estabeleceu uma conexão entre relatividade geral e
superfícies mínimas compactas, sem bordo e de índice um, ou seja, instáveis,
obtidas por métodos Min-max~\cite{CruzMinmaxminimalsurfaces2022}. Nos exemplos
conhecidos, tais superfícies coincidem com o horizonte cosmológico.

Um outro desenvolvimento recente na teoria de superfícies mínimas, são os
estudos de superfícies mínimas ilimitadas (não compactas) em espaços relevantes
para a Relatividade Geral. Estudos esses motivados pelo papel crucial que tais
objetos geométricos desempenham na prova do teorema de massa positiva exibida
por Schoen e Yau~\cite{Schoenproofpositivemass1979}. Dentre esses
desenvolvimentos, destacam-se os realizados por Carlotto, Chodosh e
Eichmair~\cite{CarlottoRigiditystableminimal2016,
  CarlottoEffectiveversionspositive2016}, em que eles demonstram que conjuntos
de dados iniciais simétricos no tempo, assintoticamente euclidianos e com
curvatura escalar positiva ou com curvatura escalar não-negativa e
assintoticamente schwarzschildiana, não contêm superfícies mínimas ilimitadas,
estáveis e mergulhadas. Esses conjuntos de dados iniciais são de interesse para a
Relatividade Geral, porque modelam sistemas gravitacionais isolados e as
condições sobre a curvatura escalar realizam a condição de energia dominante na
ausência da constante cosmológica, isto é, \(\Lambda = 0\).

Por outro lado, Chodosh e
Ketover~\cite{ChodoshAsymptoticallyflatthreemanifolds2018} demonstraram que
existe pelo menos um plano mínimo propriamente mergulhado passando por qualquer
ponto interior em um conjunto de dados iniciais simétrico no tempo e
assintoticamente euclidiano, que não contêm superfícies mínimas, compactas e sem
bordo, ou seja, na ausência de buracos negros, existe uma quantidade infinita
desses planos em sistemas gravitacionais isolados, que podem ser modelados por
conjuntos de dados simétricos no tempo. Este resultado foi significativamente
melhorado posteriormente por Mazet e
Rosenberg~\cite{MazetMinimalplanesasymptotically2022}, à saber, eles obtiveram
mais informações sobre esses planos:
\begin{itemize}
\item existe um desses planos passando por quaisquer três pontos no conjunto de
  dados iniciais;
\item existe um desses planos tangenciando qualquer plano no espaço tangente de
   de qualquer ponto no conjunto de dados iniciais.
\end{itemize}

\subsection{Sensores geometrogravitacionais}

Essas informações adicionais obtidas por Mazet e Rosenberg são particularmente
interessantes para o desenvolvimento da pesquisa teórica em geometria
diferencial e Relatividade Geral, porque elas suscitam a ideia de que esses
objetos geométricos, as superfícies mínimas ilimitadas, possam ser usadas na
teoria da Relatividade Geral para construir \emph{sensores geométricos para
  prospectar a gravidade}. Essa ideia é parcialmente validada pelos estudos de
Carlotto~\cite[Teorema 1]{CarlottoRigiditystableminimal2016}, onde ele
estabelece que o espaço euclidiano tridimensional é o único conjunto de dados
inciais simétricos no tempo, com curvatura escalar não-negativa e
assintoticamente schwarzschildiana, que admite superfícies mínimas ilimitadas,
propriamente mergulhadas e estáveis, ou seja, a presença de superfícies mínimas
ilimitadas, propriamente mergulhadas e instáveis é um indicador de que um
conjunto de dados iniciais simétrico no tempo modela um sistema
gravitacionalmente isolado não-trivial. De acordo com o \emph{teorema da massa
  positiva}, essa não trivialidade pode ser quantificada em termos da
\emph{massa ADM}, um invariante geométrico global, que quantifica a energia
gravitacional de um sistema gravitacionalmente isolado, que satisfaz a condição
de energia dominante na ausência da constante cosmológica.

A proposta de construção desses \emph{sensores geometrogravitacionais} também
encontra suporte nas contribuições de Rafael Montezuma (UFC), Ezequiel Barbosa
(UFMG) e Moya~\cite{MontezumaFreeBoundaryMinimalSurfaces2021,
  BarbosaProperfreeboundaryminimal2022} a teoria de superfícies mínimas
ilimitadas. Nas suas contribuições, motivados pelo programa de desenvolvimento
de uma teoria min-max que contemple as superfícies mínimas ilimitadas, eles
investigam essas superfícies na Schwarzschild riemanniana tridimensional, um
conjunto de dados iniciais rotacionalmente simétrico, que ocorre na solução de
Schwarzschild, uma solução exata para a equação de Einstein, sem constante
cosmológica, que é um modelo fundamental no estudo de buracos negros, estrelas e
outros corpos astrofísicos estáticos. Mais precisamente, para cada número real
\(m \ge 0\), a \emph{Schwarzschild riemanniana tridimensional}
\(\left( \mathbb{S}c^3, g \right)\) é o exterior de uma bola aberta de raio
\(m/2\), centrada na origem do espaço euclidiano tridimensional
\(\mathbb{S}c^3 \equiv \mathbb{R}^3 - \mathbb{B}^3_{m/2}(0)\), munido com a
métrica de Schwarzschild riemanniana:
\begin{align*}
  g = \left( 1 + \frac{m}{2|x|} \right)^4 \delta,
\end{align*}
uma métrica riemanniana esfericamente simétrica e conformemente plana, onde
\(\delta\) denota a métrica euclidiana canônica, \(|x|\) representa a distância
euclidiana de um ponto \(x \in \mathbb{S}c\) ao centro da bola
\(\mathbb{B}^3_{m/2}(0)\) e \(m\) é a massa ADM. O bordo \(\partial\mathbb{S}c^3\)
é uma esfera bidimensional canônica de raio de área \(2m\) e totalmente
geodésica em \(\mathbb{S}c^3\); ela é a única superfície mínima compacta, sem
bordo e estável que ocorre nesse conjunto de dados iniciais e representa o
horizonte aparente do buraco negro de Schwarzschild, que coincide com uma seção
transversal horizonte de eventos desse objeto astrofísico, por isso costuma-se
dizer que o bordo é o \emph{horizonte} desse conjunto de dados inciais.

De fato, essa é uma família a um parâmetro de conjuntos de dados iniciais
simétricos no tempo, parametrizados pela massa ADM.
Rafael Montezuma~\cite[Prop. 3.1]{MontezumaFreeBoundaryMinimalSurfaces2021} caracteriza
a parte exterior à bola \(\mathbb{B}^3_{m/2}(0)\) dos planos euclidianos
passando pelo seu centro, como os únicos cones mínimos sobre o horizonte em
\(\mathbb{S}c^3\), em particular, eles são \emph{planos mínimos propriamente
  mergulhados em \(\mathbb{S}c^3\), com bordo livre no horizonte}; também
calcula explicitamente~\cite[Teorema 1.1]{%
  MontezumaFreeBoundaryMinimalSurfaces2021} o índice deles e mostra que são
\emph{instáveis e de índice 1}. Ademais, ele
calcula~\cite[Teorema 1.2]{MontezumaFreeBoundaryMinimalSurfaces2021} que o raio
euclidiano \(R\) do domínio anelar maximal de estabilidade desses cones mínimos
em \(\mathbb{S}c^3\) é aproximadamente \(R \approx 5.5m\). Isso corrobora a
ideia de que \emph{superfícies mínimas ilimitadas são sensíveis a influência
  gravitacional}. Por fim, ele ainda obtém uma desigualdade
geométrica~\cite[Teorema 1.3]{MontezumaFreeBoundaryMinimalSurfaces2021}
que também corrobora essa interpretação acerca da sensibilidade gravitacional, à
saber: o comprimento \(|\partial\Sigma|\) do bordo de uma superfície mínima
\(\Sigma\) propriamente mergulhada em \(\mathbb{S}c^3\) e com bordo livre no
horizonte, satisfaz a desigualdade
\begin{align*}
  |\partial\Sigma| \le 4\pi m \Theta(\Sigma),
\end{align*}
onde \(\Theta(\Sigma)\) é a densidade da superfície mínima \(\Sigma\) no
infinito; e mais, se a igualdade ocorre, \(\Sigma\) é um cone mínimo sobre o
horizonte e a densidade no infinito é \(\Theta(\Sigma) = 1\). Por sua vez,
Ezequiel Barbosa e David Moya
demonstraram~\cite[Teorema 1.1]{BarbosaProperfreeboundaryminimal2022} a
existência de uma família a um parâmetro de superfícies mínimas propriamente
mergulhadas, com fronteira livre no horizonte, rotacionalmente simétricas e que
não são totalmente geodésicas; isso implica que os cones mínimos não são as
únicas superfícies que realizam as hipóteses da desigualdade geométrica, logo,
nas superfícies de Ezequiel-Moya, a desigualdade é necessariamente estrita.

\section{Atividades propostas}

\subsection{Pesquisa}

O projeto de pesquisa proposto consiste em desenvolver a teoria de superfícies
mínimas ilimitadas, com o objetivo de construir sensores geometrogravitacionais,
isto é, sensores geométricos para prospecção da gravidade na teoria da
Relatividade Geral.

Como uma primeira etapa desse programa de pesquisa, eu pretendo investigar como
as propriedades geométricas, topológicas e analíticas de uma superfície mínima
ilimitada, por exemplo, o tamanho do domínio anelar maximal estável, a quantidade
de fins e o índice, se relacionam com os parâmetros físicos dos conjuntos de
dados iniciais simétricos no tempo. Como exemplo de parâmetros físicos de
interesse para esse programa, podemos considerar a carga eletromagnética, a
massa ADM, o momento angular e a constante cosmológica, que são quantidades
físicas que, na teoria de Relatividade Geral, sabe-se que influenciam e sofrem
influência da gravidade, aqui entendida como uma manifestação da curvatura. Eu
espero que a investigação dessas relações em soluções exatas para as equações
de vínculos, que possam servir de exemplos basilares, desempenhem um papel
importante como guia nessa etapa, especialmente aqueles conjuntos de dados
iniciais que ocorrem na solução exata da Equação de Einstein conhecida como
\(\Lambda\)-Kerr-Newman, bem como seus casos particulares. Em suma, nessa etapa
eu pretendo responder a pergunta:
\begin{description}
\item[Etapa 1] Em conjuntos de dados iniciais simétricos no tempo, podemos
  expressar as propriedades geométricas, topológicas e analíticas de superfícies
  mínimas ilimitadas, em termos de quantidades de interesse físico que
  influenciam e sofrem influência da curvatura?
\end{description}
O conhecimento adquirido nessa etapa, deve reunir evidências suficientemente
robustas, para sanar qualquer dúvida que, porventura, ainda possa restar sobre a
viabilidade do programa de pesquisa e deve indicar suas eventuais limitações.
Dessa forma, poderemos passar a segunda etapa do programa.

Na segunda etapa, ainda no cenário de conjuntos de dados inciais simétricos no
tempo, eu pretendo investigar como as superfície mínimas ilimitadas podem ser
usadas para quantificar a intensidade da gravidade. O ponto de partida para essa
investigação será o estudo das superfícies mínimas ilimitadas, propriamente
mergulhadas e com bordo livre nas ditas \emph{superfícies de sondagem da atração
  da gravidade}~\footnote{Attractive Gravity Probe Surface (AGPS),
  na língua inglesa.}, isto é, superfícies compactas, sem bordo, possivelmente
desconexas, de curvatura média positiva \(H > 0\) e que, para alguma constante
real fixada \(\alpha > -\frac{1}{2}\), satisfazem a desigualdade diferencial
\begin{align*}
  \frac{\partial H}{\partial \nu} \ge \alpha H^2,
\end{align*}
ao longo do fluxo local pelo inverso pela curvatura média~\footnote{A
  positividade da curvatura média garante a existência do fluxo.}, onde \(\nu\)
é o campo normal unitário com respeito ao qual a curvatura média é calculada.
Essas classe de superfícies foram introduzidas recentemente por Izumi, Shiromizu,
Tomikawa e Yoshino~\cite{IzumiAreaboundsurfaces2021}, de forma que o parâmetro
\(\alpha\) funcione como um quantificador quasi-local da intensidade da
gravidade, isto é, na vizinhança dessas superfícies. Nessa investigação eu
espero poder estabelecer vínculos entre as propriedades de superfícies mínimas
ilimitadas, com bordo livre e o parâmetro \(\alpha\). Baseado nas contribuições
dessas pessoas~\cite{IzumiAreaboundsurfaces2021}, isso deve decorrer de
aplicações da teoria do fluxo pelo inverso da curvatura média em espaços
riemannianos e da teoria do fluxo conforme de métricas, desenvolvida por Hubert
Bray na sua prova da conjectura de Penrose. Em suma, nessa etapa eu pretendo
responder a pergunta:
\begin{description}
\item[Etapa 2] Podemos estabelecer relações quantitativas entre as propriedades
  de superfícies mínimas ilimitadas, com bordo livre, e o parâmetro \(\alpha\)
  das AGPS's?
\end{description}
Com a execução dessa etapa, eu espero ter obtido conhecimento o suficiente para
estabelecer as superfícies mínimas ilimitadas como sensores
geometrogravitacionais, no cenário de conjuntos de dados iniciais simétricos no
tempo, de forma que eu possa passar a etapa seguinte.

Nessa terceira e última etapa dessa proposta de pesquisa, eu pretendo investigar
o comportamento dinâmico das superfícies mínimas ilimitadas no desenvolvimento
de Cauchy de um conjunto de dados iniciais. Isso será feito considerando uma
folheação do espaço-tempo por uma família a um parâmetro de
\emph{hipersuperfícies de Cauchy}. Nessa etapa eu pretendo responder a pergunta:
\begin{description}
\item[Etapa 3] Qual o comportamento dinâmico das superfícies mínimas ilimitadas
  do ponto de vista do espaço-tempo?
\end{description}

\subsection{Ensino}

A proposta de atividades de ensino tem por objetivo formar e atrair recursos
humanos qualificados a contribuirem com desenvolvimento da Relatividade Geral
Matemática. Assim, proponho ofertar anualmente as disciplinas:
\begin{itemize}
\item Relatividade Geral, CBP8055, 06 créditos;
\item Seminário de Pesquisa em Relatividade Geral, 02 créditos.
\end{itemize}

A primeira disciplina faz parte do leque de disciplinas avançadas do Mestrado
Acadêmico e do leque de disciplinas optativas do Doutorado Acadêmico da
Pgmat/UFC; o plano da disciplina elaborado pelo proponente será submetido para
apreciação da coordenação da Pgmat/UFC no momento oportuno.

Por sua vez, a segunda disciplina será ofertada no semestre seguinte a oferta da
primeira; a proposta dessa disciplina é dar continuidade a formação, com foco no
desenvolvimento de competências técnicas e sócio-emocionais; a metodologia
empregada será a exposição, a discussão e a elaboração de sínteses de artigos e
outros textos que tratem de temas de interesse para a pesquisa na área; dentre
outros, os recursos utilizados serão o assistente pessoal de pesquisa Zotero e o
método de gerenciamento pessoal de conhecimento Zettelkasten.

Além do desenvolvimento de competências, a metodologia e os recursos empregados
nessa disciplina, tem por objetivo potencializar a emergência de produções e
colaborações científicas.

\subsection{Eventos}

A proposta de organização de eventos tem por objetivo fomentar a socialização, o
intercâmbio e a difusão de conhecimento na área de Relatividade Geral Matemática
e é de fundamental importância para o desenvolvimento acadêmico da área local e
regionalmente. Assim, na medida da disponibilidade dos recursos da Pgmat/UFC ou
captados de outras fontes, proponho organizar os seguintes eventos:
\begin{itemize}
  \item Seminários Híbridos Mensais usando a plataforma Conferência Web da
    RNP.br;
  \item Dia da Relatividade Geral: evento presencial semestral com quatro
    pessoas palestrantes convidadas~\footnote{Evento inspirado num formato que
      já foi testado com sucesso na Pgmat/UFC.};
  \item Sessão Temática da área a ser submetida para apreciação do comitê
    científico do VII Congresso Latino Americano y del Caribe de Matemática -
    CLAM 2024.
\end{itemize}

A qualidade científica, a interdisciplinaridade, bem como a diversidade, no
sentido mais amplo possível, e a representatividade de grupos sub-representados,
serão diretrizes aplicadas na organização dos eventos.

\newpage
\bibliographystyle{utphys}
\bibliography{projeto}

\end{document}